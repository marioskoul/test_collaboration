import System.Environment

(|>) x f = f x
(|$) x f = f <$> x

primes n =
  [2..]
    |> filter (\i ->
        [2..(i - 1)]
          |> all ((/= 0) . (i `mod`)))
    |> take n

main =
  getArgs
  |$ head
  |$ read
  |$ primes
  |$ show
  >>= putStrLn
